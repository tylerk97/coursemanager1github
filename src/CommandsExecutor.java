// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import java.util.ArrayList;

/**
 * This class will make calls to execute
 * the methods based on the section.
 * 
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 */

public class CommandsExecutor {
    // private Student student;
    //private Section type;
    private Section type1;
    private Section type2;
    private Section type3;
    private ArrayList<String> commandList;
    //private int sectionNumber;


    /**
     * Constructor for CommandsExecutor
     * 
     * @param commandList
     *            the list of commands from the parser
     */
    public CommandsExecutor(ArrayList<String> commandList2) {
        this.commandList = commandList2;
        //type = new Section(sectionNumber);
        type1 = new Section(1);
        type2 = new Section(2);
        type3 = new Section(3);


        this.Execution(commandList);

    }


    /**
     * The method that will go through all lines collected
     * from CommandFileParser. Calls will be made to other
     * functions in this class by section, and the calls
     * will implemented in Section.java to apply to that
     * particular section.
     * 
     * @param commandList
     *            the list of commands from the parser
     */
    public void Execution(ArrayList<String> commandList) {
        Section currType = type1; // default section is 1;
        int integerType = 1;

        for (int i = 0; i < commandList.size(); i++) {
            String currLine = commandList.get(i).trim(); // current line
            int score = 0; // initialize new score variable

            if (commandList.get(i).contains("section 1")) {
                //String sectionNumber = "[ ]+"; // "+" indicates the number of delimiters
                // doesn't matter
                //String[] tokens = currLine.split(sectionNumber);
                //integerType = Integer.parseInt(tokens[1]);
                
                
                
                //type = tokens[1]; // change to type 1
                currType = type1;
                integerType = 1;
                continue; // goes to next array element
            }

            else if (commandList.get(i).contains("section 2")) {
                currType = type2; // change to type 2
                integerType = 2;
                continue; // goes to next array element
            }

            else if (commandList.get(i).contains("section 3")) {
                currType = type3; // change to type3
                integerType = 3;
                continue; // goes to next array element
            }

            else if (commandList.get(i).contains("insert")) {
                String names = "[ ]+"; // "+" indicates the number of delimiters
                                       // doesn't matter
                String[] tokens = currLine.split(names);

                this.insert(tokens[1], tokens[2], currType);
                continue; // inserts and goes to next array element
            }

            else if (commandList.get(i).contains("search")) {
                String names = "[ ]+"; // "+" indicates the number of delimiters
                                       // doesn't matter
                String[] tokens = currLine.split(names);

                // search and one name
                if (tokens.length == 2) {
                    this.search(tokens[1], currType);
                    continue; // searches and goes to next array element
                }

                // search and two names
                else if (tokens.length == 3) { // search and two names
                    this.search(tokens[1], tokens[2], currType);
                    continue; // searches and goes to next array element
                }
            }

            // added space after remove to indicate not removesection
            else if (commandList.get(i).contains("remove ")) {
                String names = "[ ]+"; // "+" indicates the number of delimiters
                                       // doesn't matter
                String[] tokens = currLine.split(names);

                this.remove(tokens[1], tokens[2], currType);
                continue; // inserts and goes to next array element
            }

            else if (commandList.get(i).contains("score")) {
                String scoreLine = "[ ]+"; // "+" indicates the number of
                                           // delimiters doesn't matter
                String[] tokens = currLine.split(scoreLine);
                score = Integer.parseInt(tokens[1]);
                

                this.score(score, currType);
                continue; // inserts and goes to next array element
            }

            else if (commandList.get(i).contains("removesection")) {
                this.removesection(integerType, currType);
                continue; // removes and goes to next array element
            }

            else if (commandList.get(i).contains("dumpsection")) {
                this.dumpsection(currType);
                continue; // removes and goes to next array element
            }

            else if (commandList.get(i).contains("grade")) {
                this.grade(currType);
                continue; // removes and goes to next array element
            }

        }
    }


    /**
     * Will insert a student if they are not already
     * inserted into the tree.
     * 
     * @param firstName
     *            the first name of the student
     * @param lastName
     *            the last name of the student
     * @param type
     *            the current section
     */
    public void insert(String firstName, String lastName, Section type) {
        type.insert(firstName, lastName);
    }


    /**
     * Will print out an error message if the full name cannot be
     * found. Will print out the student record (name and id) if
     * the full name is found.
     * 
     * @param firstName
     *            the first name of the student
     * @param lastName
     *            the last name of the student
     * @param type
     *            the current section
     */
    public void search(String firstName, String lastName, Section type) {
        type.search(firstName, lastName);
    }


    /**
     * This command is only valid when it follows an insert command or a
     * successful search command. If valid, it sets the score to a student.
     * If there are multiple student records from a search record, this
     * method is considered invalid.
     * 
     * @param score
     *            the score to be assigned
     * @param type
     *            the current section
     */
    public void score(int score, Section type) {
        type.score(score);
    }


    /**
     * Helper function to help determine if score() can be called validly
     * 
     * @return T/F
     */
    public boolean validScore() {
        // return search;
        return true;
    }


    /**
     * Removes the record in the current section with the
     * requested name
     * 
     * @param firstName
     *            the first name of the student
     * @param lastName
     *            the last name of the student
     * @param type
     *            the current section
     */
    public void remove(String firstName, String lastName, Section type) {
        type.remove(firstName, lastName);
    }


    /**
     * Remove all records associated with a section (01, 02, or 03). If
     * not section is provided, the default section would be
     * the current section. The section still exists, but the records
     * will be removed.
     * 
     * @param section
     *            the specified section (integer)
     * @param type
     *            the current section
     */
    public void removesection(int section, Section type) {
        type.removesection(section);
    }


    /**
     * Reports all students in the current section that have a requested
     * name (first or last). The number of records with the name will
     * be reported.
     * 
     * @param name
     *            the name to be searched for
     * @param type
     *            the current section
     */
    public void search(String name, Section type) {
        type.search(name);

        // boolean -> T will be valid for score, F will not
    }


    /**
     * Returns a “dump” of the BST associated with the current section.
     * The drump prints out each BST node (use the in-order traversal).
     * For each BST node, that node's student record is printed.
     * The size of the BST is also printed.
     * 
     * @param type
     *            the current section
     */
    public void dumpsection(Section type) {
        type.dumpsection();
    }


    /**
     * Assigns grades to all students in the BST in the current section.
     * Reports how many students are in each grade level.
     * 
     * @param type
     *            the current section
     */
    public void grade(Section type) {
        type.grade();
    }


    /**
     * Reports all student pairs whose scores are within a difference
     * given by the "difference" input. If no difference is given, then
     * the pairs should have exactly the same score. Returns a number
     * of how many pairs were found.
     * 
     * @param difference
     *            the difference to create a range of scores to be found.
     * @param type
     *            the current section
     */
    public void findpair(int difference, Section type) {
        type.findpair(difference);
    }
}
