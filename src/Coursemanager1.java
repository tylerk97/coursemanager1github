// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Class that runs the main program
 * 
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class Coursemanager1 {

    /**
     * This main() method will run the entire program
     * 
     * @param args
     *            the input to run the program
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        CommandFileParser parser;
        if (args.length >= 1) {
            parser = new CommandFileParser(args[0]);
        }
        parser = new CommandFileParser(args[0]);
        ArrayList<String> command = parser.readFileList(args[0]);
        CommandsExecutor execute = new CommandsExecutor(command);

    }

}
