// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import student.TestCase;

/**
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class SectionTest extends TestCase {

    private BST2<Student> spiffBST;
    private Student spiffs;
    private Section section;


    /**
     * The setUp method that creates a BST for all test cases
     */
    public void setUp() {

        section = new Section(1);

        spiffBST = new BST2<Student>();
        spiffs = new Student("Spaceman", "Spiffs", 85, 0001, 01);
        // section = new Section(spiffs, 1);
    }


    /**
     * Tests the insert method. If a student already
     * has the same name, then they will not be added
     * and a statement is printed
     */
    public void testInsert() {

        Student bois = new Student("Calvin", "Hobbes", 90, 0002, 01);

// System.out.println(section.insert("Calvin", "Hobbes"));
// System.out.println(section.search("Spaceman", "Spiffs"));
        /**
        System.out.println(section.insert("Calvin", "Hobbes"));
        System.out.println(section.search("Calvin", "Hobbes"));
        System.out.println(section.size());
        System.out.println(section.insert("Spaceman", "Spiff"));
        System.out.println(section.search("Spaceman", "Spiff"));
        System.out.println(section.size());
        System.out.println(section.insert("Calvin", "Hobbes"));
        System.out.println(section.size());
        assertEquals(section.getSize(), 2);

        section.insert("Spaceman", "Spiffs");
        spiffBST.insert(spiffs);
        section.insert(bois);
        */
        
// assertEquals(spiffs.getFirstName(), "Spaceman");
//
// record.append("0001, Spaceman Spiffs, score = 85");
// // assertEquals(spiffBST.search("Spaceman", "Spiffs"), record);
//
// record.append("Student Calvin Boomer doesn't exist in section 0001");
// //assertEquals(section.search("Calvin", "Boomer"), record);

        // assertEquals(spiffs.getFirstName(), "Spaceman");

        // assertEquals(section.insert("Spacemn", "Spiffs"), "Spacemn Spiffs
        // inserted");
        // assertEquals(section.insert(bois), "Spaceman Spiffs inserted");

        // assertEquals(spiffBST.size(), 3);

        // not solved...

    }

// public void testSearch() {
// Student student = new Student("Fringe", "Guy");
// //System.out.println(section.search("Fringe", "Guy"));
// //assertEquals(section.search("Fringe", "Guy"), "not found");
// section.insert(student);
// section.printBST();
// System.out.println(section.search("Fringe", "Guy"));
// assertEquals(section.search("Fringe", "Guy"), "found");
// }

}
