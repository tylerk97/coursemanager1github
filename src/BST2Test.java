// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import student.TestCase;

/**
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class BST2Test extends TestCase {

    private BST2<String> spiffBST;
    // private BST2<Integer> idBST;
    //private Student spiffs;


    /**
     * Set up method to create empty BST for all test cases
     */
    public void setUp() {
        spiffBST = new BST2<String>();
        // spiffs = new Student("Spaceman", "Spiffs", 85, 1, 1);
    }


    /**
     * tests the clear, isClear, insert, remove, and size methods
     * to make sure they are working correctly
     */
    public void testTreeManipulations() {
        // nothing in tree
        assertEquals(spiffBST.size(), 0);
        assertTrue(spiffBST.isClear());

        // insert one elements, size is 1
        spiffBST.insert("spiffs");
        assertEquals(spiffBST.size(), 1);

        // insert three elements to test size
        spiffBST.insert("spaceman");
        spiffBST.insert("calvin");
        spiffBST.insert("hobbes");

        // elements inside of tree, size is 4
        assertEquals(spiffBST.size(), 4);
        assertFalse(spiffBST.isClear());

        // elements removed, size is 2
        spiffBST.remove("spaceman");
        spiffBST.remove("hobbes");
        assertEquals(spiffBST.size(), 2);
        assertFalse(spiffBST.isClear());

        // elements cleared
        spiffBST.clear();
        assertTrue(spiffBST.isClear());
    }


    /**
     * tests the find method to find a specific element
     * within a given tree
     */
    public void testFind() {
        spiffBST.insert("spiffs");
        spiffBST.insert("spaceman");
        spiffBST.insert("calvin");
        spiffBST.insert("hobbes");

        assertEquals("calvin", spiffBST.find("calvin"));
        assertNull(spiffBST.find("pizza"));
        
        BST2<Student> test = new BST2<Student>();
        Student student = new Student("Calvin", "Hobbes");
        Student student2 = new Student("Calvin2", "Hobbes2");
        Student student3 = new Student("Calvin3", "Hobbes3");
        Student tester = new Student("Calvin", "Hobbes");
        
        test.insert(student3);
        test.insert(student2);
        test.insert(student);
        assertEquals(test.find(tester).getLastName(), "Hobbes");
    }


    /**
     * tests the getMax() and deleteMax() methods
     */
    public void testMaxMethods() {
        spiffBST.insert("spiffs");
        spiffBST.insert("spaceman");
        spiffBST.insert("calvin");
        spiffBST.insert("hobbes");

        assertEquals("calvin", spiffBST.find("calvin"));

        // assertEquals(spiffBST.getMax(spiffBST.);

    }

    /**
     * tests hasNext(), next(), and getStack() methods
     * when stack is empty
     */
    // public void testIteratorEmpty() {
    // Iterator<T> iter = spiffBST.iterator();
    // assertEquals(false, iter.hasNext());

    // Exception exception = null;
    // try {
    // iter.next();
    // }
    // catch (NoSuchElementException e) {
    // exception = e;
    // }
    // assertTrue(exception instanceof NoSuchElementException);
    // spiffBST.insert("spiffs");
    // spiffBST.insert("spaceman");
    // spiffBST.insert("calvin");
    // spiffBST.insert("hobbes");

    // Iterator<Node> iter2 = spiffBST.iterator();
    // assertTrue(iter2.hasNext());
    // assertEquals(s3, iter2.next());
    // assertEquals(s2, iter2.next());
    // assertEquals(s1, iter2.next());

    // assertFalse(iter2.hasNext());

    // }

}
