// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import java.util.*;

/**
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 * @param <T>
 */
public class BST2<T extends Comparable<T>> implements Iterator<T> {

    private Node<T> root; // Root of the BST
    private int nodecount; // Number of nodes in the BST


    /**
     * The BST2 constructor
     */
    BST2() {
        root = null;
        nodecount = 0;
    }


    /**
     * Reinitialize tree
     */
    public void clear() {
        root = null;
        nodecount = 0;
    }


    /**
     * Returns true for a cleared tree and false for a populated one
     * 
     * @return T/F
     */
    public boolean isClear() {
        return (root == null && nodecount == 0);
    }


    /**
     * Insert a record into the tree.
     * Records can be anything, but they must be Comparable
     * 
     * @param e
     *            The record to insert.
     */
    public void insert(T e) {
        root = inserthelp(root, e);
        nodecount++;
    }


    /**
     * Remove a record from the tree
     * Returns the record removed, null if there is none
     * 
     * @param key
     *            The key value of record to remove
     * @return temp
     */
    public T remove(T key) {
        T temp = findhelp(root, key); // First find it
        if (temp != null) {
            root = removehelp(root, key); // Now remove it
            nodecount--;
        }
        return temp;
    }


    /**
     * Return the record with key value k, null if none exists
     * 
     * @param key
     *            the key to be found
     * @return the element at the key
     */
    public T find(T key) {
        return findhelp(root, key);
    }


    /**
     * Return the number of records in the dictionary
     * 
     * @return nodecount
     */
    public int size() {
        return nodecount;
    }


    /**
     * helper method for insert method
     * 
     * @param rt
     *            right tree node
     * @param e
     *            the key being used
     * @return right tree node to be inserted
     */
    private Node<T> inserthelp(Node<T> rt, T e) {
        if (rt == null) {
            return new Node<T>(e);
        }
        if (rt.value().compareTo(e) >= 0) {
            rt.setLeft(inserthelp(rt.left(), e));
        }
        else {
            rt.setRight(inserthelp(rt.right(), e));
        }
        return rt;
    }


    /**
     * helper method for find method
     * 
     * @param rt
     *            right tree node
     * @param key
     *            the key being used
     * @return element that is desired
     */
    private T findhelp(Node<T> rt, T key) {
        if (rt == null) {
            return null;
        }
        if (rt.value().compareTo(key) > 0) {
            return findhelp(rt.left(), key);
        }
        else if (rt.value().compareTo(key) == 0) {
            return rt.value();
        }
        else {
            return findhelp(rt.right(), key);
        }
    }


    /**
     * helper method for remove method
     * 
     * @param rt
     *            right tree node
     * @param key
     *            key being used
     * @return the element to be removed
     */
    private Node<T> removehelp(Node<T> rt, T key) {
        if (rt == null) {
            return null;
        }
        if (rt.value().compareTo(key) > 0) {
            rt.setLeft(removehelp(rt.left(), key));
        }
        else if (rt.value().compareTo(key) < 0) {
            rt.setRight(removehelp(rt.right(), key));
        }
        else {
            if (rt.left() == null) {
                return rt.right();
            }
            else if (rt.right() == null) {
                return rt.left();
            }
            else {
                Node<T> temp = getMax(rt.left());
                rt.setValue(temp.value());
                rt.setLeft(deleteMax(rt.left()));
            }
        }
        return rt;
    }
    



    /**
     * Method to delete the max node
     * 
     * @param rt
     *            right tree node
     * @return rt the node to be deleted
     */
    private Node<T> deleteMax(Node<T> rt) {
        if (rt.right() == null) {
            return rt.left();
        }
        rt.setRight(deleteMax(rt.right()));
        return rt;
    }


    /**
     * Finds the max node in the tree
     * 
     * @param rt
     *            the right tree node
     * @return rt the max node
     */
    private Node<T> getMax(Node<T> rt) {
        if (rt.right() == null) {
            return rt;
        }
        return getMax(rt.right());
    }


    /**
     * Binary tree node implementation: supports comparable objects
     * 
     * @author <Tim Beliveau> <tbel23>
     * @author <Tyler Kurowski> <tylerk97>
     * @version <06.03.2020>
     * @param <T>
     */
    class Node<T> {
        private T element; // Tlement for this node
        private Node<T> left; // Pointer to left child
        private Node<T> right; // Pointer to right child


        /**
         * Constructor
         */
        Node() {
            left = null;
            right = null;
        }


        /**
         * Constructor
         * 
         * @param val
         *            value for element
         */
        Node(T val) {
            left = null;
            right = null;
            element = val;
        }


        /**
         * Constructor
         * 
         * @param val
         *            value for element
         * @param l
         *            left node
         * @param r
         *            right node
         */
        Node(T val, Node<T> l, Node<T> r) {
            left = l;
            right = r;
            element = val;
        }


        /**
         * Get and set the element value
         * 
         * @return element
         */
        public T value() {
            return element;
        }


        /**
         * Set the element value
         * 
         * @param v
         *            the value being set for the element
         */
        public void setValue(T v) {
            element = v;
        }


        /**
         * Getter for left child
         * 
         * @return left node
         */
        public Node<T> left() {
            return left;
        }


        /**
         * Setter for left child
         * 
         * @param p
         *            the element being set to the left child
         */
        public void setLeft(Node<T> p) {
            left = p;
        }


        /**
         * Getter for right child
         * 
         * @return right child
         */
        public Node<T> right() {
            return right;
        }


        /**
         * Setter for the right child
         * 
         * @param p
         *            element being set to the right child
         */
        public void setRight(Node<T> p) {
            right = p;
        }


        /**
         * return TRUE if a leaf node, FALSE otherwise
         * 
         * @return T/F
         */
        public boolean isLeaf() {
            return (left == null) && (right == null);
        }
    }


    /**
     * Iterator to traverse tree
     * 
     * @return new iterator
     */
    public Iterator<T> iterator() {
        return new BSTIterator();
    }


    /**
     * Private iterator class to traverse tree
     * 
     * @author <Tim Beliveau> <tbel23>
     * @author <Tyler Kurowski> <tylerk97>
     * @version <06.03.2020>
     */
    private class BSTIterator implements Iterator<T> {
        private Stack<Node<T>> nodeStack;
        private Node<T> curr = nodeStack.peek();


        /**
         * Constructor for iterator
         */
        public BSTIterator() {
            if (root != null) {
                nodeStack = new Stack<Node<T>>();
                goLeftFrom(root);
            }
        }


        /**
         * Method to determine if there is another node
         * to be found in the tree.
         */
        @Override
        public boolean hasNext() {
            if (nodeStack == null || nodeStack.isEmpty()) {
                return false;
            }
            return (nodeStack.peek().element != null);
        }


        /**
         * Traverses to the next element in the tree
         */
        @Override
        public T next() {
            if (!nodeStack.empty()) {
                nodeStack.pop();
                if (curr.right != null) {
                    goLeftFrom(curr.right);
                }
            }
            return (curr.element);
        }


        /**
         * Traverses according to in-order method
         */
        public void inOrder() {
            inOrderHelper(root);
        }


        /**
         * Helper method for inOrder
         * 
         * @param t
         *            the current node
         */
        private void inOrderHelper(Node<T> t) {
            if (curr == null) {
                return;
            }
            inOrderHelper(t.left);
            System.out.println(t.element);
            inOrderHelper(t.right);
        }


        /**
         * Helper method to push nodes all the way to the left
         * until there is no left child
         * 
         * @param t
         *            the current node
         */
        private void goLeftFrom(Node<T> t) {
            while (t != null) {
                nodeStack.push(t);
                t = t.left;
            }
        }

        // @Override
        // public T next() {
        // if (nodeStack.isEmpty()) {
        // return null;
        // }
        // else {
        // Node<T> curr = nodeStack.peek();
        // nodeStack.pop();

        // if (curr.right != null) {
        // pushToLeft(curr.right);
        // }
        // return curr.element;
        // }
        // }


        /**
         * Function that pushes nodes along the left. Tnds when node has no
         * child.
         * 
         * @param t
         *            the current node being pushed
         */
        // private void pushToLeft(Node<T> t) {
        // while (t != null) {
        // nodeStack.push(t);
        // t = t.left;
        // }
        // }

        /**
         * Getter for the node stack
         * 
         * @return the node stack
         */
        public Stack<Node<T>> getStack() {
            return nodeStack;
        }
    }


    @Override
    public boolean hasNext() {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public T next() {
        // TODO Auto-generated method stub
        return null;
    }
}
