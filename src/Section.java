// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



/**
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class Section extends BST2<Student> {

    private BST2<Student> bst;
    // private Student currStudent;
    private int id;
    private int size;


    /**
     * Constructor for section class
     * 
     * @param section
     *            the section number
     */
    public Section(int section) {
        bst = new BST2<Student>();
        this.id = section;
    }


    /**
     * Will insert a student if they are not already
     * inserted into the tree.
     * 
     * @param firstName
     *            the first name of the student
     * @param lastName
     *            the last name of the student
     * @return confirmation, a string with the resulting action
     */
    public void insert(String firstName, String lastName) {
        Student newStudent = new Student(firstName, lastName);
        StringBuilder confirmation = new StringBuilder();

        if (this.search(firstName, lastName).equals("not found")) {
            bst.insert(newStudent);
            size = bst.size() + 1;
            // size++;
            confirmation.append(newStudent.getFirstName() + " " + newStudent
                .getLastName() + " inserted");
            
            System.out.println(confirmation.toString());
        }
        else {
            confirmation.append(newStudent.getFirstName() + " " + newStudent
                .getLastName() + " is already in section " + id);

            confirmation.append("\n");

            confirmation.append(newStudent.getId() + ", " + newStudent
                .getFirstName() + " " + newStudent.getLastName() + " "
                + "score =" + newStudent.getScore());
            
            System.out.println(confirmation.toString());
        }
    }

    // if (bst.size() == 0) {
    // bst.insert(newStudent);
    // size++;
    // return "inserted";
    // }

// StringBuilder confirmation = new StringBuilder();
// Student student = new Student(firstName, lastName);
//
// while (bst.hasNext()) {
// if (bst.next().compareTo(student) != 0) {
// bst.insert(student);
// confirmation.append(student.getFirstName() + " " + student
// .getLastName() + " inserted");
//
// System.out.println(student.getFirstName() + " " + student
// .getLastName() + " inserted");
// }
// else {
// confirmation.append(student.getFirstName() + " " + student
// .getLastName() + " is already in section " + id);
//
// System.out.println(student.getFirstName() + " " + student
// .getLastName() + " is already in section " + id);
// }
// }
// return confirmation.toString();


    /**
     * Returns the size of the current BST.
     * 
     * @return size of BST
     */
    public int getSize() {
        return bst.size();
    }


    /**
     * Sets the size of the current BST.
     * 
     * @param newSize
     *            new size of BST
     */
    public void setSize(int newSize) {
        this.size = newSize;
    }


    /**
     * Will print out an error message if the full name cannot be
     * found. Will print out the student record (name and id) if
     * the full name is found.
     * 
     * @param firstName
     *            the first name of the student
     * @param lastName
     *            the last name of the student
     * @return record, the student's record
     */
    public String search(String firstName, String lastName) {

        Student testStudent = new Student(firstName, lastName);
        // System.out.println(bst.find(testStudent));
        if (bst.find(testStudent) != null) {
            return "found";
        }
        else {
            return "not found";
        }
    }


    /**
     * This command is only valid when it follows an insert command or a
     * successful search command. If valid, it sets the score to a student.
     * If there are multiple student records from a search record, this
     * method is considered invalid.
     * 
     * @param score
     *            the score to be assigned
     */
    public void score(int score) {
        // todo
        // student.setScore(score);

    }


    /**
     * Helper function to help determine if score() can be called validly
     * 
     * @return T/F
     */
    public boolean validScore() {
        // return search;
        return true;
    }


    /**
     * Removes the record in the current section with the
     * requested name
     * 
     * @param firstName
     *            the first name of the student
     * @param lastName
     *            the last name of the student
     */
    public void remove(String firstName, String lastName) {
        // Student student = new Student(firstName, lastName);
        // StringBuilder record = new StringBuilder();

        // student.remove();
    }


    /**
     * Remove all records associated with a section (01, 02, or 03). If
     * not section is provided, the default section would be
     * the current section. The section still exists, but the records
     * will be removed.
     * 
     * @param section
     *            the specified section
     */
    public void removesection(int section) {

        // student.removesection();
    }


    /**
     * Reports all students in the current section that have a requested
     * name (first or last). The number of records with the name will
     * be reported.
     * 
     * @param name
     *            the name to be searched for
     */
    public void search(String name) {

        // boolean -> T will be valid for score, F will not
        // student.search();
    }


    /**
     * Returns a “dump” of the BST associated with the current section.
     * The drump prints out each BST node (use the in-order traversal).
     * For each BST node, that node's student record is printed.
     * The size of the BST is also printed.
     */
    public void dumpsection() {
        // bst.dumpsection();
        //todo
    }


    /**
     * Assigns grades to all students in the BST in the current section.
     * Reports how many students are in each grade level.
     */
    public void grade() {
        // bst.createStringOfGrades();
    }


    /**
     * Reports all student pairs whose scores are within a difference
     * given by the "difference" input. If no difference is given, then
     * the pairs should have exactly the same score. Returns a number
     * of how many pairs were found.
     * 
     * @param difference
     *            the difference to create a range of scores to be found.
     */
    public void findpair(int difference) {
        // student.findpair();
    }
}
