// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



/**
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class Student implements Comparable<Student> {
    private String firstName;
    private String lastName;
    private int score;
    private int id;
    private int section;


    /**
     * Constructor for student class
     * 
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param score
     *            the score
     * @param id
     *            the id
     * @param section
     *            the section
     */
    public Student(
        String firstName,
        String lastName,
        int score,
        int id,
        int section) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.score = score;
        this.id = id;
        this.section = section;
    }


    /**
     * Constructor for student class
     * 
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     */
    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.score = 0;
        this.id = 0;
        this.section = 1;
    }


    /**
     * Method to get the full id of a student.
     * The first two digits refer to the section (01, 02, or 03),
     * and the last four refer to the student number, which is
     * determined based on when the student is added
     * 
     * @return fullId, the fullId of the student
     */
    public String getId() {
        StringBuilder fullId = new StringBuilder();
        fullId.append("0");
        // first two digits will be from section
        int first = this.section;
        // last four digits will count from 0000 and up
        int second = this.id;

        // add in loop here or in Section.java to count students

        // add padded zeroes
        // String formattedFirst = String.format("%02d", first);
        String formattedSecond = String.format("%04d", second);
        String fullnumber = first + formattedSecond;

        id = Integer.parseInt(fullnumber);
        fullId.append(id);

        return fullId.toString();
    }


    /**
     * Method to set a new id for a student
     * 
     * @param newId
     *            the newId for the student
     */
    public void setId(int newId) {
        this.id = newId;
    }


    /**
     * Getter to find a student's first name
     * 
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Setter to rename a student's first name
     * 
     * @param newFirstName
     *            new first name
     */
    public void setFirstName(String newFirstName) {
        this.firstName = newFirstName;
    }


    /**
     * Getter to find a student's last name
     * 
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Setter to rename a student's last name
     * 
     * @param newLastName
     *            new last name
     */
    public void setLastName(String newLastName) {
        this.lastName = newLastName;
    }


    /**
     * Getter to find a student's score
     * 
     * @return score
     */
    public int getScore() {
        return score;
    }


    /**
     * Setter to set a student's score
     * 
     * @param newScore
     *            the new score
     */
    public void setScore(int newScore) {
        this.score = newScore;
    }


    /**
     * Method that will compare two students.
     * Same firstName and lastName -> 0
     * Null names -> -1
     * Different name in any way -> 1
     * 
     * @param student
     *            the student being compared
     * @return number, the possibilities are above
     */
    @Override
    public int compareTo(Student student) {
        if (student.lastName == null || student.firstName == null) {
            return -2;
        }

        if (student.lastName.compareTo(this.lastName) == 0 && student.firstName
            .compareTo(this.firstName) == 0) {
            return 0;
        }

        else if (student.lastName.compareTo(this.lastName) == 0
            && student.firstName.compareTo(this.firstName) == 1) {
            return 1;
        }
        else if (student.lastName.compareTo(this.lastName) == 0
            && student.firstName.compareTo(this.firstName) == -1) {
            return -1;
        }
        else if (student.lastName.compareTo(this.lastName) == -1) {
            return -1;
        }
        else {
            return 1;
        }
    }

}
