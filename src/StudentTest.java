// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import student.TestCase;

/**
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class StudentTest extends TestCase {

    private Student spiffs;
    private Student compareKid;
    private Student basicKid;
    private Student basicKid2;


    /**
     * Sets up example Student for all test cases
     */
    public void setUp() {
        spiffs = new Student("Spaceman", "Spiffs", 85, 1, 1);
        compareKid = new Student("Spaceman", "Spiffs", 85, 1, 1);

        basicKid = new Student("Gravity", "Bois");
        basicKid2 = new Student("Gravity", "Bois");

    }


    /**
     * Tests the Id getter and setters to make sure
     * they are working correctly with padded zeroes.
     */
    public void testGetId() {
        assertEquals("010001", spiffs.getId());
        assertEquals("010000", basicKid.getId());
    }


    /**
     * Tests the getters and setters for name methods
     */
    public void testNames() {
        assertEquals("Spaceman", spiffs.getFirstName());
        assertEquals("Spiffs", spiffs.getLastName());

        spiffs.setFirstName("Calvin");
        spiffs.setLastName("Hobbes");
        basicKid.setFirstName("Shield");
        basicKid.setLastName("Gang");

        assertEquals("Calvin", spiffs.getFirstName());
        assertEquals("Hobbes", spiffs.getLastName());
        assertEquals("Shield", basicKid.getFirstName());
        assertEquals("Gang", basicKid.getLastName());
    }


    /**
     * Tests the getters and setters for score methods
     */
    public void testScore() {
        assertEquals(85, spiffs.getScore());
        assertEquals(0, basicKid.getScore());

        spiffs.setScore(90);
        assertEquals(90, spiffs.getScore());
    }


    /**
     * Tests the compareTo() method to make sure the correct
     * comparisons are being made.
     */
    public void testCompareTo() {
        // Students have the same full name
        assertEquals(0, spiffs.compareTo(compareKid));

        // First name will be different
        compareKid.setFirstName("shieldgang");
        assertEquals(1, spiffs.compareTo(compareKid));

        // Full name will be different
        compareKid.setLastName("sadbois");
        assertEquals(1, spiffs.compareTo(compareKid));

        // Null Last name example
        compareKid.setLastName(null);
        assertEquals(-2, spiffs.compareTo(compareKid));

        // Null First name example
        compareKid.setLastName("gravitybois");
        compareKid.setFirstName(null);
        assertEquals(-2, spiffs.compareTo(compareKid));

        // Null Full name example
        compareKid.setLastName(null);
        assertEquals(-2, spiffs.compareTo(compareKid));

        assertEquals(1, spiffs.compareTo(basicKid));

    }

}
