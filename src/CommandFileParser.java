// On my honor:
//
// - I have not used source code obtained from another student,
// or any other unauthorized source, either modified or
// unmodified.
//
// - All source code and documentation used in my program is
// either my original work, or was derived by me from the
// source code published in the textbook for this course.
//
// - I have not discussed coding details about this project with
// anyone other than my partner (in the case of a joint
// submission), instructor, ACM/UPE tutors or the TAs assigned
// to this course. I understand that I may discuss the concepts
// of this program with other students, and that another student
// may help me debug my program so long as neither of us writes
// anything during the discussion or modifies any computer file
// during the discussion. I have violated neither the spirit nor
// letter of this restriction.
// -- Tyler Kurowski (tylerk97)
// -- Tim Beliveau (tbel23)



import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class will parse the input file and keep
 * record of all commands to be executed.
 * 
 * @author <Tim Beliveau> <tbel23>
 * @author <Tyler Kurowski> <tylerk97>
 * @version <06.03.2020>
 *
 */
public class CommandFileParser {
    private ArrayList<String> fileList;


    /**
     * Constructor to set up the bst and file to be read
     * 
     * @param fileName
     *            the input file
     * @throws FileNotFoundException
     */
    public CommandFileParser(String fileName) throws FileNotFoundException {
        fileList = new ArrayList<String>();
        // fileList = readFileList(fileName);
    }


    /**
     * The method that will parse through the input file and record
     * all commands.
     * 
     * @param fileName
     *            the input file
     * @return bst with a record of commands
     * @throws FileNotFoundException
     */
    public ArrayList<String> readFileList(String fileName)
        throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(fileName));
        // bst = new BST2<String>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            fileList.add(line);
        }

        scanner.close();

        return fileList;

    }

}
